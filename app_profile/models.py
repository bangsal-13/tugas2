from django.db import models
from django.contrib.auth.models import User
import datetime

YEAR_CHOICES = []
for r in range(1900, (datetime.datetime.now().year+1)):
    YEAR_CHOICES.append((r,r))

LEVEL_CHOICES = (
	('',''),
	('Beginner','Beginner'),
	('Intermediate','Intermediate'),
	('Advanced','Advanced'),
	('Expert','Expert'),
	('Legend','Legend'),
)

class Teman(models.Model):
	NPM = models.CharField(max_length=10)
	Nama = models.TextField()
	Angkatan = models.IntegerField(choices=YEAR_CHOICES, default=datetime.datetime.now().year)
	Skill1 = models.TextField()
	Skill2 = models.TextField()
	Level1 = models.CharField(max_length=1, choices=LEVEL_CHOICES, default='1')
	Level2 = models.CharField(max_length=1, choices=LEVEL_CHOICES, default='1')
# Create your models here.
class Status(models.Model):
	message = models.CharField(max_length=100)
	created_date = models.DateTimeField(auto_now_add=True)

class Profile(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)

	kode_identitas = models.CharField(max_length=20, primary_key=True,)
	name = models.CharField('Nama', max_length=300, null=True, blank=True)
	email = models.EmailField('Email', null=True, blank=True)
	profile_url = models.URLField('Profile Linkedin', null=True, blank=True)
	image_url = models.URLField('Foto', null=True, blank=True)
